﻿namespace WPSJS加载项安装辅助工具ByExcel催化剂
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtWpsAppDir = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt7zFilePath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPluginsXmlPath = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.AddinName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddinInstallType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddinType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.AddinUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddinVersion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddinState = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSelect7zFile = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.chkShowWebDebugger = new System.Windows.Forms.CheckBox();
            this.btnQuit = new System.Windows.Forms.Button();
            this.cmbAddinType = new System.Windows.Forms.ComboBox();
            this.btnAddJspluginsXml = new System.Windows.Forms.Button();
            this.btnOpenINI = new System.Windows.Forms.Button();
            this.btnOpenPlugins = new System.Windows.Forms.Button();
            this.btnDelteAddins = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(849, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "WPS安装路径（若识别失败需手动输入，最后部分为版本号文件夹如：D:\\Program Files (x86)\\WPS Office\\11.1.0.10356）";
            // 
            // txtWpsAppDir
            // 
            this.txtWpsAppDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWpsAppDir.Location = new System.Drawing.Point(12, 36);
            this.txtWpsAppDir.Name = "txtWpsAppDir";
            this.txtWpsAppDir.Size = new System.Drawing.Size(898, 25);
            this.txtWpsAppDir.TabIndex = 1;
            this.txtWpsAppDir.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtWpsAppDir_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 304);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(743, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "离线安装包类型及路径，输入后按回车追加记录（后缀为7z，当网址地址时为离线版，本地路径为脱机离线版）";
            // 
            // txt7zFilePath
            // 
            this.txt7zFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt7zFilePath.Location = new System.Drawing.Point(129, 324);
            this.txt7zFilePath.Name = "txt7zFilePath";
            this.txt7zFilePath.Size = new System.Drawing.Size(637, 25);
            this.txt7zFilePath.TabIndex = 1;
            this.txt7zFilePath.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt7zFilePath_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 241);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(726, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "jsplugins.xml路径（输入后按回车追加记录，用于追加合并其插件清单到本机上，同名插件会覆盖原有的）";
            // 
            // txtPluginsXmlPath
            // 
            this.txtPluginsXmlPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPluginsXmlPath.Location = new System.Drawing.Point(12, 266);
            this.txtPluginsXmlPath.Name = "txtPluginsXmlPath";
            this.txtPluginsXmlPath.Size = new System.Drawing.Size(754, 25);
            this.txtPluginsXmlPath.TabIndex = 1;
            this.txtPluginsXmlPath.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPluginsXmlPath_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(12, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(898, 159);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "加载项安装模式说明";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(592, 15);
            this.label9.TabIndex = 0;
            this.label9.Text = "脱机离线模式：插件安装包在本地，所有安装过程无需联网，需在下方提供离线包路径。";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(772, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "离线模式：所有文件都在服务器上，仅在首次或版本有更新时，才需要联网下载文件到本地，其余时间可离线运行。";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 66);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(526, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "在线模式：所有文件都在服务器上，每次运行WPS必须联网状态才能使用插件。";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(367, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "安装模式分为在线模式、离线模式、脱机离线模式三种";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AddinName,
            this.AddinInstallType,
            this.AddinType,
            this.AddinUrl,
            this.AddinVersion,
            this.AddinState});
            this.dataGridView1.Location = new System.Drawing.Point(12, 392);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 27;
            this.dataGridView1.Size = new System.Drawing.Size(754, 280);
            this.dataGridView1.TabIndex = 3;
            // 
            // AddinName
            // 
            this.AddinName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AddinName.HeaderText = "插件名称";
            this.AddinName.Name = "AddinName";
            this.AddinName.ReadOnly = true;
            this.AddinName.Width = 75;
            // 
            // AddinInstallType
            // 
            this.AddinInstallType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AddinInstallType.HeaderText = "安装模式";
            this.AddinInstallType.Name = "AddinInstallType";
            this.AddinInstallType.Width = 75;
            // 
            // AddinType
            // 
            this.AddinType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AddinType.HeaderText = "插件类型";
            this.AddinType.Items.AddRange(new object[] {
            "表格",
            "文字",
            "演示"});
            this.AddinType.Name = "AddinType";
            this.AddinType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AddinType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.AddinType.Width = 75;
            // 
            // AddinUrl
            // 
            this.AddinUrl.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AddinUrl.HeaderText = "插件更新链接";
            this.AddinUrl.Name = "AddinUrl";
            this.AddinUrl.ReadOnly = true;
            // 
            // AddinVersion
            // 
            this.AddinVersion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AddinVersion.HeaderText = "插件版本号";
            this.AddinVersion.Name = "AddinVersion";
            this.AddinVersion.ReadOnly = true;
            this.AddinVersion.Width = 89;
            // 
            // AddinState
            // 
            this.AddinState.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.AddinState.HeaderText = "开启状态";
            this.AddinState.Name = "AddinState";
            this.AddinState.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.AddinState.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.AddinState.Width = 75;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 370);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(651, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "插件安装清单（仅脱机离线模式可设置开启状态，设置过jsplugins.xml网络路径时将会被改写）";
            // 
            // btnSelect7zFile
            // 
            this.btnSelect7zFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelect7zFile.Location = new System.Drawing.Point(791, 322);
            this.btnSelect7zFile.Name = "btnSelect7zFile";
            this.btnSelect7zFile.Size = new System.Drawing.Size(119, 43);
            this.btnSelect7zFile.TabIndex = 4;
            this.btnSelect7zFile.Text = "选择本地\n7z文件";
            this.btnSelect7zFile.UseVisualStyleBackColor = true;
            this.btnSelect7zFile.Click += new System.EventHandler(this.btnSelect7zFile_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(791, 580);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(119, 43);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "确定";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // chkShowWebDebugger
            // 
            this.chkShowWebDebugger.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowWebDebugger.AutoSize = true;
            this.chkShowWebDebugger.Location = new System.Drawing.Point(791, 392);
            this.chkShowWebDebugger.Name = "chkShowWebDebugger";
            this.chkShowWebDebugger.Size = new System.Drawing.Size(119, 19);
            this.chkShowWebDebugger.TabIndex = 6;
            this.chkShowWebDebugger.Text = "开启开发工具";
            this.chkShowWebDebugger.UseVisualStyleBackColor = true;
            // 
            // btnQuit
            // 
            this.btnQuit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQuit.Location = new System.Drawing.Point(791, 629);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(119, 43);
            this.btnQuit.TabIndex = 5;
            this.btnQuit.Text = "退出";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // cmbAddinType
            // 
            this.cmbAddinType.FormattingEnabled = true;
            this.cmbAddinType.Items.AddRange(new object[] {
            "表格",
            "文字",
            "演示"});
            this.cmbAddinType.Location = new System.Drawing.Point(15, 325);
            this.cmbAddinType.Name = "cmbAddinType";
            this.cmbAddinType.Size = new System.Drawing.Size(108, 23);
            this.cmbAddinType.TabIndex = 7;
            // 
            // btnAddJspluginsXml
            // 
            this.btnAddJspluginsXml.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddJspluginsXml.Location = new System.Drawing.Point(791, 262);
            this.btnAddJspluginsXml.Name = "btnAddJspluginsXml";
            this.btnAddJspluginsXml.Size = new System.Drawing.Size(119, 41);
            this.btnAddJspluginsXml.TabIndex = 4;
            this.btnAddJspluginsXml.Text = "选择本地\nJspluginsXml文件";
            this.btnAddJspluginsXml.UseVisualStyleBackColor = true;
            this.btnAddJspluginsXml.Click += new System.EventHandler(this.btnAddJspluginsXml_Click);
            // 
            // btnOpenINI
            // 
            this.btnOpenINI.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenINI.Location = new System.Drawing.Point(791, 422);
            this.btnOpenINI.Name = "btnOpenINI";
            this.btnOpenINI.Size = new System.Drawing.Size(119, 43);
            this.btnOpenINI.TabIndex = 5;
            this.btnOpenINI.Text = "打开oem.ini\n所在文件夹";
            this.btnOpenINI.UseVisualStyleBackColor = true;
            this.btnOpenINI.Click += new System.EventHandler(this.btnOpenINI_Click);
            // 
            // btnOpenPlugins
            // 
            this.btnOpenPlugins.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenPlugins.Location = new System.Drawing.Point(791, 469);
            this.btnOpenPlugins.Name = "btnOpenPlugins";
            this.btnOpenPlugins.Size = new System.Drawing.Size(119, 43);
            this.btnOpenPlugins.TabIndex = 5;
            this.btnOpenPlugins.Text = "打开plugins\n所在文件夹";
            this.btnOpenPlugins.UseVisualStyleBackColor = true;
            this.btnOpenPlugins.Click += new System.EventHandler(this.btnOpenPlugins_Click);
            // 
            // btnDelteAddins
            // 
            this.btnDelteAddins.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelteAddins.Location = new System.Drawing.Point(791, 531);
            this.btnDelteAddins.Name = "btnDelteAddins";
            this.btnDelteAddins.Size = new System.Drawing.Size(119, 43);
            this.btnDelteAddins.TabIndex = 5;
            this.btnDelteAddins.Text = "删除加载项";
            this.btnDelteAddins.UseVisualStyleBackColor = true;
            this.btnDelteAddins.Click += new System.EventHandler(this.btnDelteAddins_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 684);
            this.Controls.Add(this.cmbAddinType);
            this.Controls.Add(this.chkShowWebDebugger);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnOpenPlugins);
            this.Controls.Add(this.btnOpenINI);
            this.Controls.Add(this.btnDelteAddins);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnAddJspluginsXml);
            this.Controls.Add(this.btnSelect7zFile);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txtPluginsXmlPath);
            this.Controls.Add(this.txtWpsAppDir);
            this.Controls.Add(this.txt7zFilePath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.Text = "WPSJS加载项安装及管理ByExcel催化剂，QQ群：222739992";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtWpsAppDir;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt7zFilePath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPluginsXmlPath;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSelect7zFile;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.CheckBox chkShowWebDebugger;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.ComboBox cmbAddinType;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddinName;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddinInstallType;
        private System.Windows.Forms.DataGridViewComboBoxColumn AddinType;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddinUrl;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddinVersion;
        private System.Windows.Forms.DataGridViewCheckBoxColumn AddinState;
        private System.Windows.Forms.Button btnAddJspluginsXml;
        private System.Windows.Forms.Button btnOpenINI;
        private System.Windows.Forms.Button btnOpenPlugins;
        private System.Windows.Forms.Button btnDelteAddins;
    }
}

