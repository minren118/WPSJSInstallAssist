﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace WPSJS加载项安装辅助工具ByExcel催化剂
{
    public class WebClientEx : WebClient
    {
        /// <summary>  
        /// 过期时间，单位毫秒  
        /// </summary>  
        public int Timeout { get; set; }

        /// <summary>
        /// 单位毫秒
        /// </summary>
        /// <param name="timeout"></param>
        public WebClientEx(int timeout)
        {
            Timeout = timeout;
        }


        /// <summary>  
        /// 重写GetWebRequest,添加WebRequest对象超时时间  
        /// </summary>  
        /// <param name="address"></param>  
        /// <returns></returns>  
        protected override WebRequest GetWebRequest(Uri address)
        {
            HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(address);
            request.Timeout = Timeout;
            request.ReadWriteTimeout = Timeout;
            return request;
        }
    }
}
